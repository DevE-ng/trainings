package com.spr.controllers;

import com.spr.service.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/profiles")
public class ProfileResource {

    @Autowired
    EmailSender emailSender;

    @PostMapping(path = "/create")
    public ResponseEntity<String> create() {
        emailSender.send("Congratulation, profile created");
        return new ResponseEntity<>("Send", HttpStatus.OK);
    }
}
